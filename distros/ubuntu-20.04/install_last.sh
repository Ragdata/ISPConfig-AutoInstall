#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_last.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_last.sh
# Author:       Ragdata
# Date:         13/01/2021 1853
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallLast() {
  echo "DONE"
}