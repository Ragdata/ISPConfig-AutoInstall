#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_jailkit.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_jailkit.sh
# Author:       Ragdata
# Date:         13/01/2021 1847
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
# TODO - Install Jailkit (build-essential autoconf automake libtool flex bison debhelper binutils)
JKV="2.19"
SUM="f46cac122ac23b1825330d588407aa96"

InstallJailkit() {
  echo -n "Installing Jailkit... "
  pkg_install build-essential autoconf automake libtool flex bison debhelper binutils
  cd /
  wget -q https://olivier.sessink.nl/jailkit/jailkit-$JKV.tar.gz
  if [[ ! "$(md5sum jailkit-$JKV.tar.gz | head -c 32)" = "$SUM" ]]; then
    echo -e "\n${red}Error: md5sum does not match${NC}" >&2
    echo "Please try running this script again" >&2
    exit 1
  fi
  tar xfz jailkit-$JKV.tar.gz
  cd jailkit-$JKV
  echo 5 > debian/compat
  ./debian/rules binary > /dev/null 2>&1
  cd ..
  hide_output dpkg -i jailkit_$JKV-1_*.deb
  rm -rf jailkit-$JKV
  echo -e "[${green}DONE${NC}]\n"
}