#-------------------------------------------------------------------
# distros/ubuntu-20.04/askquestions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/askquestions.sh
# Author:       Ragdata
# Date:         13/01/2021 1526
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
AskQuestions() {
	if ! command -v whiptail >/dev/null; then
		echo -n "Installing whiptail... "
		pkg_install whiptail
		echo -e "[${green}DONE${NC}]\n"
	fi

	while [[ ! "$CFG_SQLSERVER" =~ $RE ]]
	do
		CFG_SQLSERVER=$(whiptail --title "SQL Server" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select SQL Server" 10 50 2 "MySQL" "" ON "MariaDB" "(default)" OFF 3>&1 1>&2 2>&3)
	done

	while [[ ! "$CFG_MYSQL_ROOT_PWD" =~ $RE ]]
	do
		CFG_MYSQL_ROOT_PWD=$(whiptail --title "$CFG_SQLSERVER" --backtitle "$WT_BACKTITLE" --passwordbox "Specify a root SQL Server password" --nocancel 10 50 3>&1 1>&2 2>&3)
	done

	while [[ ! "$CFG_WEBSERVER" =~ $RE ]]
	do
		CFG_WEBSERVER=$(whiptail --title "Web server" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select Web Server" 10 50 2 "Apache" "(default)" ON "Nginx" "" OFF 3>&1 1>&2 2>&3)
	done
	CFG_WEBSERVER=${CFG_WEBSERVER,,}

	if [[ ! "$CFG_WEBDAV" =~ $RE ]]; then
		if (whiptail --title "WebDAV Server" --backtitle "$WT_BACKTITLE" --yesno "Configure WebDAV Services?" 10 50) then
			CFG_WEBDAV=yes
		else
			CFG_WEBDAV=no
		fi
	fi

  if [[ ! "$CFG_PHPCACHE" =~ $RE ]]; then
    if (whiptail --title "PHP Accelerator" --backtitle "$WT_BACKTITLE" --yesno "Install PHP Cache? (OpCache / XCache)" 10 50) then
      CFG_PHPCACHE=yes
    else
      CFG_PHPCACHE=no
    fi
  fi



	while [[ ! "$CFG_MTA" =~ $RE ]]
	do
		CFG_MTA=$(whiptail --title "Mail Server" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select Mail Server" 10 50 2 "Dovecot" "(default)" ON "Courier" "" OFF 3>&1 1>&2 2>&3)
	done
	CFG_MTA=${CFG_MTA,,}

	while [[ ! "$CFG_HHVM" =~ $RE ]]
	do
		CFG_HHVM=$(whiptail --title "Install HHVM" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Do you want to install HHVM (Hip Hop Virtual Machine) as PHP engine?" 10 50 2 "yes" "" OFF "no""(default)" ON 3>&1 1>&2 2>&3)
	done

  if [[ ! "$CFG_XMPP" =~ $RE ]]; then
    if (whiptail --title "XMPP Server" --backtitle "$WT_BACKTITLE" --yesno "Do you want to install an XMPP Server?" 10 50) then
      while [[ ! "$CFG_XMPP" =~ $RE ]]; then
      do
        CFG_XMPP=$(whiptail --title "XMPP Server" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select XMPP Server" 10 50 2 "Jabberd" "(default)" ON "Metronome" "" OFF 3>&1 1>&2 2>&3)
      done
    else
      CFG_XMPP=no
    fi
  fi

  if [[ ! "$CFG_MEMDATA" =~ $RE ]]; then
    if (whiptail --title "In-Memory Data Store" --backtitle "$WT_BACKTITLE" --yesno "Do you want to install an In-Memory Data Store? (eg: Redis or Memcached)") then
      while [[ ! "$CFG_MEMDATA" =~ $RE ]]; then
      do
        CFG_MEMDATA=$(whiptail --title "In-Memory Data Store" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select In-Memory Data Store" 10 50 2 "Redis" "(default)" ON "Memcached" "" OFF 3>&1 1>&2 2>&3)
      done
    else
      CFG_MEMDATA=no
    fi
  fi

  while [[ ! "$CFG_GLUU" =~ $RE ]]
  do
    CFG_GLUU=$(whiptail --title "Install SSO Server" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Do you want to install a Identity Management / Single Sign-On (SSO) Server?" 10 50 2 "yes" "" ON "no" "(default)" OFF 3>&1 1>&2 2>&3)
  done

	while [[ ! "$CFG_AVUPDATE" =~ $RE ]]
	do
		CFG_AVUPDATE=$(whiptail --title "Update Freshclam DB" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Do you want to update Antivirus Database?" 10 50 2 "yes" "(default)" ON "no" "" OFF 3>&1 1>&2 2>&3)
	done

	if [[ ! "$CFG_QUOTA" =~ $RE ]]; then
		if (whiptail --title "Quota" --backtitle "$WT_BACKTITLE" --yesno "Setup user quota?" 10 50) then
			CFG_QUOTA=yes
		else
			CFG_QUOTA=no
		fi
	fi

	while [[ ! "$CFG_ISPC" =~ $RE ]]
	do
		CFG_ISPC=$(whiptail --title "ISPConfig Setup" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Would you like full unattended setup of expert mode for ISPConfig?" 10 50 2 "standard" "(default)" ON "expert" "" OFF 3>&1 1>&2 2>&3)
	done

	if [[ ! "$CFG_JKIT" =~ $RE ]]; then
		if (whiptail --title "Jailkit" --backtitle "$WT_BACKTITLE" --yesno "Would you like to install Jailkit (it must be installed before ISPConfig)?" 10 50) then
			CFG_JKIT=yes
		else
			CFG_JKIT=no
		fi
	fi

	CFG_WEBMAIL=roundcube

	while [[ ! "$CFG_PHPMYADMIN" =~ $RE ]]
	do
		CFG_PHPMYADMIN=$(whiptail --title "Install phpMyAdmin" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Do you want to install phpMyAdmin?" 10 50 2 "yes" "(default)" ON "no" "" OFF 3>&1 1>&2 2>&3)
	done

  if [[ ! "$CFG_VAULT" =~ $RE ]]; then
    if (whiptail --title "Password Vault" --backtitle "$WT_BACKTITLE" --yesno "Do you want to install Password Vault?" 10 50) then
      while [[ ! "$CFG_VAULT" =~ $RE ]]; then
        CFG_VAULT=$(whiptail --title "Password Vault" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select Password Vault" 10 50 2 "Passbolt" "(default)" ON "KeeWeb" "" OFF 3>&1 1>&2 2>&3)
      fi
    else
      CFG_VAULT=no
    fi
  fi

	while [[ ! "$CFG_NEXTCLOUD" =~ $RE ]]
	do
		CFG_NEXTCLOUD=$(whiptail --title "Install Nextcloud" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Do you want to install Nextcloud?" 10 50 2 "yes" "" ON "no" "(default) " OFF 3>&1 1>&2 2>&3)
	done

	while [[ ! "$SSL_COUNTRY" =~ $RE ]]
	do
		SSL_COUNTRY=$(whiptail --title "SSL Country" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Country Name (2 letter code) (ex. AU)" --nocancel 10 50 "${LANG:3:2}" 3>&1 1>&2 2>&3)
	done

	while [[ ! "$SSL_STATE" =~ $RE ]]
	do
		SSL_STATE=$(whiptail --title "SSL State" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - State or Province Name (full name) (ex. Queensland)" --nocancel 10 50 3>&1 1>&2 2>&3)
	done

	while [[ ! "$SSL_LOCALITY" =~ $RE ]]
	do
		SSL_LOCALITY=$(whiptail --title "SSL Locality" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Locality Name (eg, city) (ex. Brisbane)" --nocancel 10 50 3>&1 1>&2 2>&3)
	done

	while [[ ! "$SSL_ORGANIZATION" =~ $RE ]]
	do
		SSL_ORGANIZATION=$(whiptail --title "SSL Organization" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Organization Name (eg, company) (ex. Company L.t.d.)" --nocancel 10 50 3>&1 1>&2 2>&3)
	done

	while [[ ! "$SSL_ORGUNIT" =~ $RE ]]
	do
		SSL_ORGUNIT=$(whiptail --title "SSL Organization Unit" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Organizational Unit Name (eg, section) (ex. IT Department)" --nocancel 10 50 3>&1 1>&2 2>&3)
	done
}