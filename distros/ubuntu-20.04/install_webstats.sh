#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_webstats.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_webstats.sh
# Author:       Ragdata
# Date:         13/01/2021 1917
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallWebStats() {
  echo -n "Installing Statistics (Vlogger, Webalizer and AWStats)... ";
  pkg_install vlogger webalizer awstats geoip-database libclass-dbi-mysql-perl
  sed -i 's/^/#/' /etc/cron.d/awstats
  echo -e "[${green}DONE${NC}]\n"
}
