#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_hhvm.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_hhvm.sh
# Author:       Ragdata
# Date:         13/01/2021 1847
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallHHVM() {
  echo -n "Installing HHVM (Hip Hop Virtual Machine)... "
  pkg_install hhvm
  echo -e "[${green}DONE${NC}]\n"
}