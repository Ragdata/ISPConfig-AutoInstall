#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_basephp.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_basephp.sh
# Author:       Ragdata
# Date:         13/01/2021 1859
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallBasePHP() {
  # TODO - Install Basic PHP Modules ... and we're talking BASIC here
  # TODO - Could be a good place to do any post-install stuff that needs doing ... extra packages ...
  apt update
  pkg_install php php-{amqp,apcu,bcmath,bz2,cgi,cli,curl,ds,fpm,gd,gmp,imap,intl,json,ldap,mbstring,odbc,pear,pgsql,pspell,readline,sqlite3,tidy,xhprof,xml,xmlrpc,zip}
  pkg_install php-tcpdf php-fdpdf php-dompdf
}