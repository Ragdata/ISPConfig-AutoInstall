#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_quota.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_quota.sh
# Author:       Ragdata
# Date:         13/01/2021 1837
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
# TODO - Install Filesystem Quotas (quota quotatool)
InstallQuota() {

  echo -n "Installing Quota ... "
  pkg_install quota quotatool
  echo -e "[${green}DONE${NC}]\n"

  if ! [ -f /proc/user_beancounters ]; then
    echo -n "Initialising Quota, this may take a while ... "
    if [ "$(grep -c ',usrjquota=quota.user,grpjquota=quota.group,jqfmt=vfsv0' /etc/fstab)" -eq 0 ]; then
			sed -i '/\/[[:space:]]\+/ {/tmpfs/!s/errors=remount-ro/errors=remount-ro,usrjquota=quota.user,grpjquota=quota.group,jqfmt=vfsv0/}' /etc/fstab
			sed -i '/\/[[:space:]]\+/ {/tmpfs/!s/defaults/defaults,usrjquota=quota.user,grpjquota=quota.group,jqfmt=vfsv0/}' /etc/fstab
		fi
		mount -o remount /
		quotacheck -avugm
		quotaon -avug
		echo -e "[${green}DONE${NC}]\n"
  fi
}