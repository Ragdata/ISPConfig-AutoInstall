#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_webdav.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_webdav.sh
# Author:       Ragdata
# Date:         13/01/2021 1853
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
# TODO - Make changes to ISPConfig Directory Skeleton once installed
InstallWebDAV() {
  if [ "$CFG_WEBSERVER" == "Apache" ]; then

    mkdir /var/www/webdav
    chown -R www-data: /var/www

    a2enmod dav
    a2enmod dav_fs

    entry="DavLockDB /var/www/DavLock"
    sed -i "1s/^/${entry}\n/" /etc/apache2/sites-available/000-default.conf

  elif [ "$CFG_WEBSERVER" == "Nginx" ]; then

    sed -i '/gzip_/ s/#\ //g' /etc/nginx/nginx.conf

    cp $APWD/distros/$DISTRO/webdav.conf /etc/nginx/conf.d/webdav.conf

    sed -i "s/yourDomain;/${CFG_HOSTNAME_FQDN}/" /etc/nginx/conf.d/webdav.conf

    echo -n 'userName:' | tee -a /etc/nginx/.credentials.list;
    openssl passwd -apr1 | tee -a /etc/nginx/.credentials.list;

  fi
}