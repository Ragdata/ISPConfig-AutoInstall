#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_dns.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_dns.sh
# Author:       Ragdata
# Date:         13/01/2021 1915
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallBind() {
  echo -n "Installing DNS server (Bind)... ";
  pkg_install bind9 dnsutils haveged
  echo -e "[${green}DONE${NC}]\n"
}
