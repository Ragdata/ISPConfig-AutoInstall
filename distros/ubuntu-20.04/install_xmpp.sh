#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_xmpp.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_xmpp.sh
# Author:       Ragdata
# Date:         13/01/2021 1850
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#-------------------------------------------------------------------
InstallXMPP() {
  if [ "$CFG_XMPP" == "Jabberd" ]; then

    pkg_install ejabberd

    ejabberdctl register admin localhost Ausi186275

    cp $APWD/distros/$DISTRO/ejabberd.yml /etc/ejabberd/.

    chown root:ejabberd /etc/ejabberd/ejabberd.pem
    chmod 640 /etc/ejabberd/ejabberd.pem

    service ejabberd restart

  elif [ "$CFG_XMPP" == "Metronome" ]; then

    echo -n "Installing Metronome... ";
    pkg_install git lua5.1 liblua5.1-0-dev lua-filesystem libidn11-dev libssl-dev lua-zlib lua-expat lua-event lua-bitop lua-socket lua-sec luarocks luarocks
    luarocks install lpc
    adduser --no-create-home --disabled-login --gecos 'Metronome' metronome
    cd /opt; git clone https://github.com/maranda/metronome.git metronome
    cd ./metronome; ./configure --ostype=debian --prefix=/usr
    make
    make install
    cd /etc/metronome/certs && make localhost.key && make localhost.csr && make localhost.cert && chmod 0400 localhost.key && chown metronome localhost.key
    echo -e "[${green}DONE${NC}]\n"

  fi
}