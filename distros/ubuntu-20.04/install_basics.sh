#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_basics.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_basics.sh
# Author:       Ragdata
# Date:         13/01/2021 0828
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallBasics() {
  echo -n "Updating apt package database and upgrading currently installed packages ... "
  hide_output apt update
  hide_output apt dist-upgrade -y
  hide_output apt autoremove -y
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Adding common repository ... "
  hide_output add-apt-repository -y ppa:ondrej/common
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Adding repository for PHP ... "
  hide_output add-apt-repository -y ppa:ondrej/php
  echo -e "[${green}DONE${NC}]\n"

  if [ "$CFG_WEBSERVER" == "Apache" ]; then
    echo -n "Adding repository for Apache2 ... "
    hide_output add-apt-repository -y ppa:ondrej/apache2
    echo -e "[${green}DONE${NC}]\n"
  else
    echo -n "Adding repository for Nginx ... "
    hide_output add-apt-repository -y ppa:ondrej/nginx
    echo -e "[${green}DONE${NC}]\n"
  fi

  echo -n "Installing basic packages (OpenSSH Server, NTP, BinUtils, etc.) ... "
  pkg_install ssh openssh-server
  pkg_install nano sudo git lsb-release
  pkg_install ntp rkhunter debconf-utils binutils
  pkg_install php7.2-cli libruby php-pear
  pkg_install python3.9 python3-pip software-properties-common python-software-properties
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Stopping AppArmor ... "
  service apparmor stop
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Disabling AppArmor ... "
  hide_output update-rc.d -f apparmor remove
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Removing AppArmor ... "
  pkg_remove apparmor apparmor-utils
  echo -e "[${green}DONE${NC}]\n"

  if [ /bin/sh -ef /bin/dash ]; then
    echo -n "Changing the default shell from dash to bash ... "
    echo "dash dash/sh boolean false" | debconf-set-selections
    dpkg-reconfigure -f noninteractive dash > /dev/null 2>&1
    echo -e "[${green}DONE${NC}]\n"
  fi
}