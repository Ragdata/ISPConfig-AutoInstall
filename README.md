# ISPConfig 3+ AutoInstaller
[![Build Status](https://gitlab.com/Ragdata/ISPConfig-AutoInstall/badges/master/pipeline.svg)](https://gitlab.com/Ragdata/ISPConfig-AutoInstall/tree/master)
****
This is a script to automate the installation of ISPConfig 3+ and a host of additional packages that I use personally to configure the machines I work with, and I thought that the community may benefit from having access as well - so here it is!
## Newsletter


## Requirements
A fresh, clean install of any of the supported operating systems - preferably at DigitalOcean

## Installation / Use
If you trust me, run the following commands as a user with sudo privileges:
````
sudo curl https://ispconfig.ragdata.net | sh
````
If you don't (and you shouldn't), clone the repo and run it as a user with sudo privileges like this:
````
cd /tmp
sudo git clone https://github.com/Ragdata/ISPConfig-AutoInstall.git
cd ISPConfig-AutoInstall
sudo ./install.sh
````

## Version History
[Semantic Versioning 2.0.0](https://semver.org/)
****

#### Current Version:
**v.1.0.0**

#### Version History:
v.1.0.0:    First Full Version
* Supports Ubuntu 20.04 Only

v.0.0.1:    Initial Commit

## Contributors
****
* Primary Developer: Ragdata <ragdata@users.noreply.github.com>
* Based on the work of Matteo Temporini <temporini.matteo@gmail.com> and the community supporting his [ispconfig_setup](https://github.com/servisys/ispconfig_setup) project
* And, of course, the members of the [ISPConfig Autoinstaller](https://git.ispconfig.org/ispconfig/ispconfig-autoinstaller) project at [ispconfig.org](https://www.ispconfig.org)