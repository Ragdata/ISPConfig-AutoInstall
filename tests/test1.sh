#!/usr/bin/env bash
#-------------------------------------------------------------------
# tests/test1.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         tests/test1.sh
# Author:       Ragdata
# Date:         13/01/2021 0758
# License:      GNU GPLv3
#-------------------------------------------------------------------
# BASH COLOURS
red='\e[0;31m'
green='\e[0;32m'
yellow='\e[0;33m'
bold='\e[1m'
underlined='\e[4m'
NC='\e[0m' # No Color
COLUMNS=$(tput cols)

clear

# Check if user running script is root
if [[ $(id -u) -ne 0 ]]; then
  echo -e "${red}ERROR: This script must be run as root${NC}" >&2
  exit 1
fi

# Check if on Linux
if ! echo "$OSTYPE" | grep -iq "linux"; then
  echo -e "${red}ERROR: This script must be run on Linux${NC}" >&2
  exit 1
fi

# Check RAM
TOTAL_MEM=$(awk '/^MemTotal:/ {print $2}' /proc/meminfo)
TOTAL_SWAP=$(awk '/^SwapTotal:/ {print $2}' /proc/meminfo)
PRINT_RAM_MiB=$(printf "%'d" $((TOTAL_MEM / 1024)))
PRINT_RAM_MB=$(printf "%'d" $((((TOTAL_MEM * 1024) / 1000) / 1000)))

if [ "$TOTAL_MEM" -lt 524288 ]; then
  echo "This machine has: $PRINT_RAM_MiB MiB (PRINT_RAM_MB) RAM (memory)"
  echo -e "\n${red}ERROR: ISPConfig needs at least 512 MiB RAM, with 1 GiB (1024 MiB) recommended"
  exit 1
fi

# Check Connectivity
echo -n "Checking internet connection ... "

if ! ping -q -c 3 www.ispconfig.org > /dev/null 2>&1; then
  echo -e "${red}ERROR: Could not reach ispconfig.org - please check your internet connection and run the script again${NC}" >&2
  exit 1
fi

echo -e "[${green}DONE${NC}]\n"

# Check if ISPConfig is already installed
# TODO - In future, I'll make this an upgrade pathway
if [ -f /usr/local/ispconfig/interface/lib/config.inc.php ]; then
  echo -e "${red}ERROR: ISPConfig is already installed${NC}" >&2
  echo 1
fi
#-------------------------------------------------------------------
# GLOBAL VARIABLES
#-------------------------------------------------------------------
CFG_HOSTNAME_FQDN=$(hostname -f);
IP_ADDRESS=( $(hostname -I) );
RE='^2([0-4][0-9]|5[0-5])|1?[0-9][0-9]{1,2}(\.(2([0-4][0-9]|5[0-5])|1?[0-9]{1,2})){3}$'
IPv4_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
RE='^[[:xdigit:]]{1,4}(:[[:xdigit:]]{1,4}){7}$'
IPv6_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
WT_BACKTITLE="ISPConfig 3+ AutoInstaller"
# Save Current Working Directory
APWD=$(pwd);
#-------------------------------------------------------------------
# LOAD FUNCTIONS
#-------------------------------------------------------------------
source $APWD/functions/check_system.sh
echo -n "Checking your system, please wait ... "
CheckSystem
echo -e "[${green}DONE${NC}]\n"
